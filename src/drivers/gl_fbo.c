/**
 * $Id$
 *
 * GL framebuffer-based driver shell. This requires the EXT_framebuffer_object
 * extension, but is much nicer/faster/etc than pbuffers when it's available.
 * This is (optionally) used indirectly by the top-level GLX driver.
 *
 * Strategy-wise, we maintain 2 framebuffers with up to 4 target colour
 * buffers a piece. Effectively this reserves one fb for display output,
 * and the other for texture output (each fb has a single size).
 *
 * Copyright (c) 2005 Nathan Keynes.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#define GL_GLEXT_PROTOTYPES 1

#include <stdlib.h>
#include "lxdream.h"
#include "display.h"
#include "drivers/video_gl.h"
#include "pvr2/glutil.h"

#define MAX_FRAMEBUFFERS 2
#define MAX_TEXTURES_PER_FB 16

static render_buffer_t gl_fbo_create_render_buffer( uint32_t width, uint32_t height, GLuint tex_id );
static void gl_fbo_destroy_render_buffer( render_buffer_t buffer );
static gboolean gl_fbo_set_render_target( render_buffer_t buffer );
static void gl_fbo_finish_render( render_buffer_t buffer );
static void gl_fbo_load_pixel_buffer( pixel_buffer_t frame, render_buffer_t buffer );
static gboolean gl_fbo_test_framebuffer( );
static gboolean gl_fbo_read_render_buffer( unsigned char *target, render_buffer_t buffer, int rowstride, int format );

static GLint gl_fbo_max_attachments = 0;
static GLint gl_fbo_depth_component;
static frame_buffer_t fbo[MAX_FRAMEBUFFERS];

#define ATTACHMENT_POINT(n) (GL_COLOR_ATTACHMENT0+(n))
static int last_used_fbo;


/***************************** Frame buffer *********************************/
static GLint fbo_gl_attach_render_buffer( frame_buffer_t fb, render_buffer_t buffer )
{
    glBindFramebuffer( GL_FRAMEBUFFER, fb->fb_id );
    int attach = -1;
    GLint tex_id = buffer->buf_id;

    for( int i = 0; i < gl_fbo_max_attachments; i++ ) {
        if( fb->cb_ids[i] == tex_id ) {
#ifdef HAVE_OPENGL_DRAW_BUFFER
            glDrawBuffer(ATTACHMENT_POINT(i));
            glReadBuffer(ATTACHMENT_POINT(i)); 
#endif
            return ATTACHMENT_POINT(i); // already attached
        } else if( fb->cb_ids[i] == -1 && attach == -1 ) {
            attach = i;
        }
    }
    if( attach == -1 ) attach = 0;

    fb->cb_ids[attach] = tex_id;
    glBindTexture( GL_TEXTURE_2D, 0 ); // Ensure the output texture is unbound
    glFramebufferTexture2D(GL_FRAMEBUFFER, ATTACHMENT_POINT(attach), 
                              GL_TEXTURE_2D, tex_id, 0 );

    /* Set draw/read buffers by default */
#ifdef HAVE_OPENGL_DRAW_BUFFER    
    glDrawBuffer(ATTACHMENT_POINT(attach));
    glReadBuffer(ATTACHMENT_POINT(attach)); 
#endif

    return ATTACHMENT_POINT(attach);
}

static gboolean fbo_gl_detach_render_buffer( frame_buffer_t fb, render_buffer_t buffer )
{
    for( int i = 0; i < gl_fbo_max_attachments; i++ ) {
        if( fb->cb_ids[i] != buffer->buf_id ) continue;

        glBindFramebuffer(GL_FRAMEBUFFER, fb->fb_id);
        glFramebufferTexture2D(GL_FRAMEBUFFER, ATTACHMENT_POINT(i), 
                                              GL_TEXTURE_2D, GL_NONE, 0 );
        fb->cb_ids[i] = -1;
        return TRUE;                 
    }
    return FALSE;
}

static void fbo_gl_setup( frame_buffer_t fb, uint32_t width, uint32_t height ) 
{
    glBindFramebuffer(GL_FRAMEBUFFER, fb->fb_id);
    GLint format = gl_fbo_depth_component;

    /* Clear out any existing texture attachments */
    for( int i = 0; i < gl_fbo_max_attachments; i++ ) {
        if( fb->cb_ids[i] != -1 ) {
            glFramebufferTexture2D(GL_FRAMEBUFFER, ATTACHMENT_POINT(i),
                    GL_TEXTURE_2D, 0, 0);
            fb->cb_ids[i] = -1;
        }
    }

    fb->width  = width;
    fb->height = height;
    if( !fb->has_ds) return;

    glBindRenderbuffer(GL_RENDERBUFFER, fb->ds_id);
    glRenderbufferStorage(GL_RENDERBUFFER, format, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                              GL_RENDERBUFFER, fb->ds_id);

    if( format == GL_DEPTH24_STENCIL8 ) {
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT,
                                  GL_RENDERBUFFER, fb->ds_id);
    }
}

static void fbo_gl_destroy( frame_buffer_t fb )
{
    glBindFramebuffer( GL_FRAMEBUFFER, 0 );
    glDeleteFramebuffers(  1, &fb->fb_id );
    glDeleteRenderbuffers( 1, &fb->ds_id );
    g_free( fb );
}

static struct frame_buffer fbo_gl_vtable = { fbo_gl_attach_render_buffer, fbo_gl_detach_render_buffer, fbo_gl_setup, fbo_gl_destroy };

static frame_buffer_t fbo_gl_create( gboolean has_depth_stencil )
{
    frame_buffer_t fb = g_malloc(sizeof(struct frame_buffer));
    memcpy( fb, &fbo_gl_vtable,  sizeof(struct frame_buffer));

    glGenFramebuffers(  1, &fb->fb_id );
    glGenRenderbuffers( 1, &fb->ds_id );

    fb->width  = -1;
    fb->height = -1;
    fb->has_ds = has_depth_stencil;
    
    for( int i = 0; i < MAX_TEXTURES_PER_FB; i++ ) {
        fb->cb_ids[i] = -1;
    }
    return fb;
}


/**
 * Construct the initial frame buffers and allocate ids for everything.
 * The render handling driver methods are set to the fbo versions.
 */
void gl_fbo_init( render_driver_t render ) 
{
    render->create_frame_buffer = fbo_gl_create;
    render->create_render_buffer = gl_fbo_create_render_buffer;
    render->destroy_render_buffer = gl_fbo_destroy_render_buffer;
    render->set_render_target = gl_fbo_set_render_target;
    render->finish_render = gl_fbo_finish_render;
    render->load_pixel_buffer = gl_fbo_load_pixel_buffer;
    render->read_render_buffer = gl_fbo_read_render_buffer;

    gl_fbo_max_attachments = glGetMaxColourAttachments();
    for( int i = 0; i < MAX_FRAMEBUFFERS; i++ ) {
        fbo[i] = render->create_frame_buffer( TRUE );
    }
    last_used_fbo = 0;

    gl_fbo_test_framebuffer();
}

void gl_fbo_shutdown()
{
    for( int i = 0; i < MAX_FRAMEBUFFERS; i++ ) {
        if( fbo[i] ) fbo[i]->destroy( fbo[i] );
    }
}

static int gl_fbo_get_framebuffer( int width, int height ) 
{
    int bufno = -1, i;
    /* find a compatible framebuffer context */
    for( i=0; i<MAX_FRAMEBUFFERS; i++ ) {
        if( fbo[i]->width == -1 && bufno == -1 ) {
            bufno = i;
        } else if( fbo[i]->width == width && fbo[i]->height == height ) {
            bufno = i;
            break;
        }
    }
    if( bufno == -1 ) {
        bufno = last_used_fbo + 1;
        if( bufno >= MAX_FRAMEBUFFERS ) {
            bufno = 0;
        }
        last_used_fbo = bufno;
    }
    if( fbo[bufno]->width == width && fbo[bufno]->height == height ) {
        glBindFramebuffer( GL_FRAMEBUFFER, fbo[bufno]->fb_id );
    } else {
        fbo[bufno]->setup( fbo[bufno], width, height );
    } 
    return bufno;
}

/**
 * Attach a texture to the framebuffer. The texture must already be initialized
 * to the correct dimensions etc.
 */
static GLint gl_fbo_attach_texture( int fbo_no, render_buffer_t buffer )
{
    return fbo[fbo_no]->attach_render_buffer( fbo[fbo_no], buffer );
}

static gboolean gl_fbo_test_framebuffer( )
{
    gboolean result = TRUE;
    glGetError(); /* Clear error state just in case */
    
    frame_buffer_t fb      = render_driver->create_frame_buffer( FALSE );
    render_buffer_t buffer = render_driver->create_render_buffer( 16, 16, 0 );

    static GLint formats[] = { GL_DEPTH24_STENCIL8, GL_DEPTH_COMPONENT24, GL_DEPTH_COMPONENT16 };
    for( int i = 0; i < 3; i++ ) {
        gl_fbo_depth_component = formats[i];
        fb->setup( fb, 16, 16 );
        fb->attach_render_buffer( fb, buffer );

        GLint status = glCheckFramebufferStatus( GL_FRAMEBUFFER );
        result = TRUE;
        if( status == GL_FRAMEBUFFER_COMPLETE ) break;

        ERROR( "Framebuffer failure for %x: %x", formats[i], status );
        result = FALSE;
    }

    if( gl_fbo_depth_component == GL_DEPTH24_STENCIL8 ) {
        render_driver->capabilities.stencil_bits = 8;
    } else {
        render_driver->capabilities.stencil_bits = 0;
        WARN( "Packed depth stencil not available - disabling shadow volumes" );
    }
    gl_check_error( "Setting up framebuffer" );

    fb->detach_render_buffer( fb, buffer );
    render_driver->destroy_render_buffer( buffer );
    fb->destroy( fb );
    
    return result;
}

static render_buffer_t gl_fbo_create_render_buffer( uint32_t width, uint32_t height, GLuint tex_id )
{
    render_buffer_t buffer = calloc( sizeof(struct render_buffer), 1 );
    buffer->width = width;
    buffer->height = height;
    buffer->tex_id = tex_id;
    if( tex_id == 0 ) {
        GLuint tex;
        glGenTextures( 1, &tex );
        buffer->buf_id = tex;
    } else {
        buffer->buf_id = tex_id;
        glBindTexture( GL_TEXTURE_2D, tex_id );
    }
    glBindTexture( GL_TEXTURE_2D, buffer->buf_id );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
    return buffer;
}

/**
 * Ensure the texture in the given render buffer is not attached to a 
 * framebuffer (ie, so we can safely use it as a texture during the rendering
 * cycle, or before deletion).
 */
static void gl_fbo_detach_render_buffer( render_buffer_t buffer )
{
    int i;
    for( i=0; i<MAX_FRAMEBUFFERS; i++ ) {
        if( fbo[i]->width == buffer->width && fbo[i]->height == buffer->height ) {
            fbo[i]->detach_render_buffer( fbo[i], buffer );
        }
    }
}

static void gl_fbo_destroy_render_buffer( render_buffer_t buffer )
{
    gl_fbo_detach_render_buffer( buffer );

    if( buffer->buf_id != buffer->tex_id ) {
        // If tex_id was set at buffer creation, we don't own the texture.
        // Otherwise, delete it now.
        GLuint tex = buffer->buf_id; 
        glDeleteTextures( 1, &tex );
    }
    buffer->buf_id = 0;
    buffer->tex_id = 0;
    free( buffer );
}

static gboolean gl_fbo_set_render_target( render_buffer_t buffer )
{
    int fb = gl_fbo_get_framebuffer( buffer->width, buffer->height );
    gl_fbo_attach_texture( fb, buffer );
    /* setup the gl context */
    glViewport( 0, 0, buffer->width, buffer->height );

    return TRUE;
}

static void gl_fbo_finish_render( render_buffer_t buffer )
{
    glFinish();
    gl_fbo_detach_render_buffer(buffer);
}

static void gl_fbo_load_pixel_buffer( pixel_buffer_t frame, render_buffer_t buffer )
{
    gl_fbo_detach();
    gl_pixel_buffer_to_tex( frame, buffer->buf_id );
}

void gl_fbo_detach()
{
    glBindFramebuffer( GL_FRAMEBUFFER, 0 );
    /* Make sure texture attachment is not a current draw/read buffer */
#ifdef HAVE_OPENGL_DRAW_BUFFER
    glDrawBuffer( GL_FRONT );
    glReadBuffer( GL_FRONT );
#endif
}    

static gboolean gl_fbo_read_render_buffer( unsigned char *target, render_buffer_t buffer, 
                                           int rowstride, int format )
{
    int fb = gl_fbo_get_framebuffer( buffer->width, buffer->height );
    gl_fbo_attach_texture( fb, buffer );
    return gl_read_render_buffer( target, buffer, rowstride, format );
}

