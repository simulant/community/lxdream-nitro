/**
 * $Id$
 *
 * Null video output driver (ie no video output whatsoever)
 *
 * Copyright (c) 2005 Nathan Keynes.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "display.h"
#include "drivers/video_gl.h"

static gboolean video_null_init( void );

static render_buffer_t video_null_create_render_buffer( uint32_t hres, uint32_t vres, GLuint tex_id )
{
    return NULL;
}

static void video_null_destroy_render_buffer( render_buffer_t buffer )
{
}

static gboolean video_null_set_render_target( render_buffer_t buffer )
{
    return TRUE;
}

static void video_null_finish_render( render_buffer_t buffer )
{
}

static void video_null_display_render_buffer( render_buffer_t buffer )
{
}

static gboolean video_null_read_render_buffer( unsigned char *target, 
                                               render_buffer_t buffer, 
                                               int rowstride, int format )
{
    return TRUE;
}



static void video_null_frame_ready(void)
{
}

static void video_null_load_pixel_buffer( pixel_buffer_t frame, 
                                          render_buffer_t buffer )
{
}

static void video_null_display_blank( uint32_t colour )
{
}

static void video_null_bind_context(void)
{
}


/***************************** Frame buffer *********************************/
static GLint fbo_null_attach_render_buffer( frame_buffer_t fb, render_buffer_t buffer )
{
    return 0;
}

static gboolean fbo_null_detach_render_buffer( frame_buffer_t fb, render_buffer_t buffer )
{
    return TRUE;
}

static void fbo_null_setup( frame_buffer_t fb, uint32_t width, uint32_t height ) 
{
}

static void fbo_null_destroy( frame_buffer_t fb )
{
    g_free(fb);
}

static struct frame_buffer fbo_null_vtable = { fbo_null_attach_render_buffer, fbo_null_detach_render_buffer, fbo_null_setup, fbo_null_destroy };

static frame_buffer_t fbo_null_create( gboolean has_depth_stencil )
{
    frame_buffer_t fb = g_malloc(sizeof(struct frame_buffer));
    memcpy( fb, &fbo_null_vtable, sizeof(struct frame_buffer));
    return fb;
}


/***************************** Vertex buffer *********************************/
#define MIN_VERTEX_ARRAY_SIZE (1024*1024)

static void *vbo_null_map( vertex_buffer_t buf, uint32_t size )
{
    buf->mapped_size = size;
    if( size < MIN_VERTEX_ARRAY_SIZE )
        size = MIN_VERTEX_ARRAY_SIZE;

    if( size > buf->capacity ) {
        g_free(buf->data);
        buf->data = g_malloc(size);
        buf->capacity = size;
    }
    return buf->data;
}

static void vbo_null_unmap( vertex_buffer_t buf )
{
}

static void vbo_null_bind( vertex_buffer_t buf )
{
}

static void vbo_null_finished( vertex_buffer_t buf )
{
}

static void vbo_null_destroy( vertex_buffer_t buf )
{
    g_free(buf->data);
    buf->data = NULL;
    g_free(buf);
}

static struct vertex_buffer vbo_null_vtable = { vbo_null_map, vbo_null_unmap, vbo_null_bind, vbo_null_finished, vbo_null_destroy };

static vertex_buffer_t vbo_null_create( )
{
    vertex_buffer_t buf = g_malloc(sizeof(struct vertex_buffer));
    memcpy( buf, &vbo_null_vtable, sizeof(struct vertex_buffer));
    return buf;
}


/***************************** Vertex bindings *********************************/
static void bindings_null_init( vertex_bindings_t map, vertex_buffer_t buf, vertex_attribute_t* attributes, int numAttribs  )
{
}

static void bindings_null_bind( vertex_bindings_t map )
{
}

static void bindings_null_finished( vertex_bindings_t map )
{
}

static void bindings_null_destroy( vertex_bindings_t map )
{
}

static struct vertex_bindings bindings_null_vtable = { bindings_null_init, bindings_null_bind, bindings_null_finished, bindings_null_destroy };

static vertex_bindings_t bindings_null_create( )
{
    vertex_bindings_t buf = g_malloc(sizeof(struct vertex_bindings));
    memcpy( buf, &bindings_null_vtable, sizeof(struct vertex_bindings));
    return buf;
}


/***************************** Driver implementation *********************************/
struct display_driver display_null_driver = { 
        "null",
        N_("Null (no video) driver"),
        video_null_init, NULL,
        NULL,
        NULL, 
        NULL,
        video_null_frame_ready,
        video_null_display_render_buffer,
        video_null_display_blank,
        video_null_bind_context,
};

static struct render_driver render_null_driver = { 
        fbo_null_create,
        video_null_create_render_buffer,
        video_null_destroy_render_buffer,
        video_null_set_render_target,
        video_null_finish_render,
        video_null_load_pixel_buffer,
        video_null_read_render_buffer,
        vbo_null_create,
        bindings_null_create,
};

static gboolean video_null_init( void )
{
    render_driver = &render_null_driver;
    return TRUE;
}
