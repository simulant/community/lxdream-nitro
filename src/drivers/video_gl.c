/**
 * $Id$
 *
 * Common GL display code that doesn't depend on a specific implementation
 *
 * Copyright (c) 2005 Nathan Keynes.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <assert.h>
#include <sys/time.h>

#include "display.h"
#include "pvr2/pvr2.h"
#include "pvr2/glutil.h"
#include "pvr2/shaders.h"
#include "drivers/video_gl.h"

uint32_t video_width, video_height;
static int video_flipped;
static float video_viewMatrix[16];
static vertex_buffer_t video_vbuf;
static vertex_bindings_t video_bindings;

struct video_vertex {
    float x,y;
    float u,v;
};

void gl_set_video_size( uint32_t width, uint32_t height, int flipped )
{
    video_width   = width;
    video_height  = height;
    video_flipped = flipped;

    defineOrthoMatrix(video_viewMatrix, video_width, video_height, 0, 65535);
}

static struct video_vertex* gl_map_video_vertices()
{ 
    if( !video_vbuf ) video_vbuf = render_driver->create_vertex_buffer();

    return video_vbuf->map(video_vbuf, 4 * sizeof(struct video_vertex));
}

static gboolean gl_build_video_vertices(struct video_vertex *video_box, gboolean invert)
{
    int x1=0,y1=0,x2=video_width,y2=video_height;
    int top = 0, bottom = 1;

    if( video_flipped ) {
        top = 1;
        bottom = 0;
    }
    if ( invert ) {
        int tmp = top;
        top = bottom; bottom = tmp;
    }

    int ah = video_width * 0.75;

    if( ah > video_height ) {
        int w = (video_height/0.75);
        x1 = (video_width - w) / 2;
        x2 -= x1;
    } else if( ah < video_height ) {
        y1 = (video_height - ah) / 2;
        y2 -= y1;
    }

    video_box[0].x = x1; video_box[0].y = y1;
    video_box[0].u = 0;  video_box[0].v = top;
    video_box[1].x = x2; video_box[1].y = y1;
    video_box[1].u = 1;  video_box[1].v = top;
    video_box[2].x = x2; video_box[2].y = y2;
    video_box[2].u = 1;  video_box[2].v = bottom;
    video_box[3].x = x1; video_box[3].y = y2;
    video_box[3].u = 0;  video_box[3].v = bottom;

    return ah != video_height;
}

static void gl_init_video_bindings()
{
     vertex_attribute_t attribs[] = {
         { var_basic_shader_in_vertex_loc,   3, GL_FLOAT, offsetof(struct video_vertex, x), sizeof(struct video_vertex) },
         { var_basic_shader_in_texcoord_loc, 2, GL_FLOAT, offsetof(struct video_vertex, u), sizeof(struct video_vertex) },
     };

     video_bindings = render_driver->create_vertex_bindings();
     video_bindings->init( video_bindings, video_vbuf, attribs, 3 );
}

void gl_framebuffer_setup()
{
    glViewport( 0, 0, video_width, video_height );
    glBlendFunc( GL_ONE, GL_ZERO );
    if( !video_bindings ) gl_init_video_bindings();

    glsl_use_basic_shader();
    glsl_set_basic_shader_view_matrix(video_viewMatrix);
    video_bindings->bind(video_bindings);
    glsl_set_basic_shader_primary_texture(0);
}


void gl_framebuffer_cleanup()
{
    glsl_clear_shader();
    video_bindings->finished( video_bindings );
    video_vbuf->finished( video_vbuf );
}

/**
 * Convert window coordinates to dreamcast device coords (640x480) using the 
 * same viewable area as gl_texture_window.
 * If the coordinates are outside the viewable area, the result is -1,-1.
 */ 
void gl_window_to_system_coords( int *x, int *y )
{
    int x1=0,y1=0,x2=video_width,y2=video_height;

    int ah = video_width * 0.75;

    if( ah > video_height ) {
        int w = (video_height/0.75);
        x1 = (video_width - w)/2;
        x2 -= x1;
    } else if( ah < video_height ) {
        y1 = (video_height - ah)/2;
        y2 -= y1;
    }
    if( *x < x1 || *x >= x2 || *y < y1 || *y >= y2 ) {
        *x = -1;
        *y = -1;
    } else {
        *x = (*x - x1) * DISPLAY_WIDTH / (x2-x1);
        *y = (*y - y1) * DISPLAY_HEIGHT / (y2-y1);
    }
}

static uint32_t clearColour;
static void gl_clear_buffer()
{
    float r = (((clearColour >> 16) & 0xFF) + 1) / 256.0f;
    float g = (((clearColour >>  8) & 0xFF) + 1) / 256.0f;
    float b = (((clearColour >>  0) & 0xFF) + 1) / 256.0f;

    glClearColor(r, g, b, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
}

void gl_texture_window( int width, int height, int tex_id, gboolean inverted )
{
    struct video_vertex* video_box = gl_map_video_vertices();
    gboolean needsClear = gl_build_video_vertices(video_box, inverted);
    video_vbuf->unmap( video_vbuf );

    // Only clear gaps when necessary
    if (needsClear) gl_clear_buffer();

    gl_framebuffer_setup();
    glBindTexture(GL_TEXTURE_2D, tex_id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // Using a triangle fan happens happens to work okay to draw a single quad
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glBindTexture(GL_TEXTURE_2D, 0);
    gl_framebuffer_cleanup();
    glFlush();
}

void gl_display_blank( uint32_t colour )
{
    clearColour = colour;
    gl_clear_buffer();
    glFlush();
}

void gl_display_render_buffer(render_buffer_t buffer)
{
    gl_texture_window( buffer->width, buffer->height, buffer->buf_id, buffer->inverted );
}


gboolean gl_init_driver( display_driver_t driver )
{
    return gl_init_render( driver );
}
