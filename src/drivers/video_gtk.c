/**
 * $Id$
 *
 * The PC side of the video support (responsible for actually displaying / 
 * rendering frames)
 *
 * Copyright (c) 2005 Nathan Keynes.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <gdk/gdkkeysyms.h>
#include <stdint.h>
#include <stdlib.h>
#include "lxdream.h"
#include "display.h"
#include "dckeysyms.h"
#include "drivers/video_gl.h"
#include "drivers/joy_linux.h"
#include "pvr2/pvr2.h"
#include "gtkui/gtkui.h"

#ifdef HAVE_GLES2
#include "drivers/video_egl.h"
#endif



GtkWidget *gtk_video_drawable = NULL;

static uint16_t video_gtk_resolve_keysym( const gchar *keysym )
{
    int val = gdk_keyval_from_name( keysym );
    if( val == GDK_KEY_VoidSymbol )
        return 0;
    return (uint16_t)val;
}

static gchar *video_gtk_keycode_to_keysym( uint16_t keycode )
{
    return g_strdup(gdk_keyval_name(keycode));
}

static uint16_t video_gtk_keycode_to_dckeysym(uint16_t keycode)
{
    if( keycode >= 'a' && keycode <= 'z' ) {
        return (keycode - 'a') + DCKB_a;
    } else if( keycode >= '1' && keycode <= '9' ) {
        return (keycode - '1') + DCKB_1;
    }
    switch(keycode) {
    case GDK_KEY_0:         return DCKB_0;
    case GDK_KEY_Return:    return DCKB_Return;
    case GDK_KEY_Escape:    return DCKB_Escape;
    case GDK_KEY_BackSpace: return DCKB_BackSpace;
    case GDK_KEY_Tab:       return DCKB_Tab;
    case GDK_KEY_space:     return DCKB_space;
    case GDK_KEY_minus:     return DCKB_minus;
    case GDK_KEY_equal:     return DCKB_equal;
    case GDK_KEY_bracketleft: return DCKB_bracketleft;
    case GDK_KEY_bracketright: return DCKB_bracketright;
    case GDK_KEY_semicolon: return DCKB_semicolon;
    case GDK_KEY_apostrophe:return DCKB_apostrophe;
    case GDK_KEY_grave : return DCKB_grave;
    case GDK_KEY_comma:     return DCKB_comma;
    case GDK_KEY_period:    return DCKB_period;
    case GDK_KEY_slash:     return DCKB_slash; 
    case GDK_KEY_Caps_Lock: return DCKB_Caps_Lock;
    case GDK_KEY_F1:        return DCKB_F1;
    case GDK_KEY_F2:        return DCKB_F2;
    case GDK_KEY_F3:        return DCKB_F3;
    case GDK_KEY_F4:        return DCKB_F4;
    case GDK_KEY_F5:        return DCKB_F5;
    case GDK_KEY_F6:        return DCKB_F6;
    case GDK_KEY_F7:        return DCKB_F7;
    case GDK_KEY_F8:        return DCKB_F8;
    case GDK_KEY_F9:        return DCKB_F9;
    case GDK_KEY_F10:       return DCKB_F10;
    case GDK_KEY_F11:       return DCKB_F11;
    case GDK_KEY_F12:       return DCKB_F12;
    case GDK_KEY_Scroll_Lock: return DCKB_Scroll_Lock;
    case GDK_KEY_Pause:     return DCKB_Pause;
    case GDK_KEY_Insert:    return DCKB_Insert;
    case GDK_KEY_Home:      return DCKB_Home;
    case GDK_KEY_Page_Up:   return DCKB_Page_Up;
    case GDK_KEY_Delete:    return DCKB_Delete;
    case GDK_KEY_End:       return DCKB_End;
    case GDK_KEY_Page_Down: return DCKB_Page_Down;
    case GDK_KEY_Right:     return DCKB_Right;
    case GDK_KEY_Left:      return DCKB_Left;
    case GDK_KEY_Down:      return DCKB_Down;
    case GDK_KEY_Up:        return DCKB_Up;
    case GDK_KEY_Num_Lock:  return DCKB_Num_Lock;
    case GDK_KEY_KP_Divide: return DCKB_KP_Divide;
    case GDK_KEY_KP_Multiply: return DCKB_KP_Multiply;
    case GDK_KEY_KP_Subtract: return DCKB_KP_Subtract;
    case GDK_KEY_KP_Add:    return DCKB_KP_Add;
    case GDK_KEY_KP_Enter:  return DCKB_KP_Enter;
    case GDK_KEY_KP_End:    return DCKB_KP_End;
    case GDK_KEY_KP_Down:   return DCKB_KP_Down;
    case GDK_KEY_KP_Page_Down: return DCKB_KP_Page_Down;
    case GDK_KEY_KP_Left:   return DCKB_KP_Left;
    case GDK_KEY_KP_Begin:  return DCKB_KP_Begin;
    case GDK_KEY_KP_Right:  return DCKB_KP_Right;
    case GDK_KEY_KP_Home:   return DCKB_KP_Home;
    case GDK_KEY_KP_Up:     return DCKB_KP_Up;
    case GDK_KEY_KP_Page_Up:return DCKB_KP_Page_Up;
    case GDK_KEY_KP_Insert: return DCKB_KP_Insert;
    case GDK_KEY_KP_Delete: return DCKB_KP_Delete;
    case GDK_KEY_backslash: return DCKB_backslash;
    case GDK_KEY_Control_L: return DCKB_Control_L;
    case GDK_KEY_Shift_L:   return DCKB_Shift_L;
    case GDK_KEY_Alt_L:     return DCKB_Alt_L;
    case GDK_KEY_Meta_L:    return DCKB_Meta_L;
    case GDK_KEY_Control_R: return DCKB_Control_R;
    case GDK_KEY_Shift_R:   return DCKB_Shift_R;
    case GDK_KEY_Alt_R:     return DCKB_Alt_R;
    case GDK_KEY_Meta_R:    return DCKB_Meta_R;
    }
    return DCKB_NONE;
}


static gboolean video_gtk_expose_callback(GtkGLArea *area, cairo_t *cr, gpointer data)
{
    //pvr2_draw_frame();
    return TRUE;
}

static void video_gtk_display_render_buffer( render_buffer_t buffer )
{
    gtk_gl_area_make_current( GTK_GL_AREA(gtk_video_drawable) );
    gtk_gl_area_attach_buffers( GTK_GL_AREA(gtk_video_drawable) );
    gl_display_render_buffer( buffer );
}

static void video_gtk_display_blank( uint32_t colour )
{
    gtk_gl_area_make_current( GTK_GL_AREA(gtk_video_drawable) );
    gtk_gl_area_attach_buffers( GTK_GL_AREA(gtk_video_drawable) );
    gl_display_blank( colour );
}


static gboolean video_gtk_resize_callback(GtkGLArea *area, gint width, gint height, gpointer data )
{
    gl_set_video_size( width, height, 0 );
    pvr2_draw_frame();
    gtk_widget_queue_draw( GTK_WIDGET(area) );
    return TRUE;
}

static void video_gtk_realize_callback(GtkGLArea *area, gpointer data)
{
    gtk_gl_area_make_current(area);
    gl_init_driver(&display_gtk_driver);
    pvr2_setup_gl_context();
}

static void video_gtk_frame_ready(void)
{
    gtk_gl_area_make_current(GTK_GL_AREA(gtk_video_drawable));
    pvr2_draw_frame();
    gtk_widget_queue_draw(gtk_video_drawable);
}

static void video_gtk_bind_context(void)
{
    gtk_gl_area_make_current(GTK_GL_AREA(gtk_video_drawable));
}

GtkWidget *video_gtk_create_drawable()
{
    GtkWidget *drawable = gtk_gl_area_new();
    gtk_widget_set_can_focus(drawable,   TRUE);
    gtk_widget_set_can_default(drawable, TRUE);

    g_signal_connect(drawable, "render",  G_CALLBACK(video_gtk_expose_callback),  NULL);
    g_signal_connect(drawable, "resize",  G_CALLBACK(video_gtk_resize_callback),  NULL);
    g_signal_connect(drawable, "realize", G_CALLBACK(video_gtk_realize_callback), NULL);

    gtk_video_drawable = drawable;
    return drawable;
}

static gboolean video_gtk_init()
{
    if( gtk_video_drawable == NULL ) return FALSE;

    GtkAllocation allocation;
    gtk_widget_get_allocation( GTK_WIDGET(gtk_video_drawable), &allocation );
    gl_set_video_size( allocation.width, allocation.height, 0 );

#ifdef HAVE_LINUX_JOYSTICK
    linux_joystick_init();
#endif
    return TRUE;
}

static void video_gtk_shutdown()
{
    if( gtk_video_drawable != NULL ) {
// TODO destroy GLArea
    }
#ifdef HAVE_LINUX_JOYSTICK
    linux_joystick_shutdown();
#endif
}

struct display_driver display_gtk_driver = { 
        "gtk",
        N_("GTK-based OpenGL driver"),
        video_gtk_init, 
        video_gtk_shutdown,
        video_gtk_resolve_keysym,
        video_gtk_keycode_to_dckeysym,
        video_gtk_keycode_to_keysym,
        video_gtk_frame_ready,
        video_gtk_display_render_buffer, 
        video_gtk_display_blank, 
        video_gtk_bind_context
};

