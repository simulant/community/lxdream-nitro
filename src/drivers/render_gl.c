/**
 * $Id$
 *
 * Common GL rendering code that doesn't depend on a specific implementation
 *
 * Copyright (c) 2005,2011 Nathan Keynes.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <assert.h>
#include <sys/time.h>

#include "display.h"
#include "lxdream.h"
#include "pvr2/pvr2.h"
#include "pvr2/glutil.h"
#include "drivers/video_gl.h"
#define GL_GLEXT_PROTOTYPES 1


/************************** vertex_buffer_object *****************************/
#define MIN_VERTEX_ARRAY_SIZE (1024*1024)

#if 0
// NOTE: Doesn't work with autosorted triangles
static void *vbo_map( vertex_buffer_t buf, uint32_t size )
{
    glBindBuffer( GL_ARRAY_BUFFER, buf->id );
     if( size > buf->capacity ) {
         glBufferData( GL_ARRAY_BUFFER, size, NULL, GL_STREAM_DRAW );
         assert( gl_check_error("Allocating vbo data") );
         buf->capacity = size;
    }
    buf->data = glMapBuffer( GL_ARRAY_BUFFER, GL_WRITE_ONLY );
    buf->mapped_size = buf->capacity;
    return buf->data;
}

static void *vbo_unmap( vertex_buffer_t buf )
{
    glUnmapBuffer( GL_ARRAY_BUFFER );
    return NULL;
}
#else
static void *vbo_map( vertex_buffer_t buf, uint32_t size )
{
    buf->mapped_size = size;
    if( size < MIN_VERTEX_ARRAY_SIZE )
        size = MIN_VERTEX_ARRAY_SIZE;

    if( size > buf->capacity ) {
        g_free(buf->data);
        buf->data = g_malloc(size);
        buf->capacity = size;
    }
    return buf->data;
}

static void vbo_unmap( vertex_buffer_t buf )
{
    glBindBuffer( GL_ARRAY_BUFFER, buf->id );
    glBufferData( GL_ARRAY_BUFFER, buf->mapped_size, buf->data, GL_STREAM_DRAW );
}
#endif

static void vbo_bind( vertex_buffer_t buf )
{
    glBindBuffer( GL_ARRAY_BUFFER, buf->id );
}

static void vbo_finished( vertex_buffer_t buf )
{
    glBindBuffer( GL_ARRAY_BUFFER, 0 );
}

static void vbo_destroy( vertex_buffer_t buf )
{
    glBindBuffer( GL_ARRAY_BUFFER, 0 );
    glDeleteBuffers( 1, &buf->id );
}

static struct vertex_buffer vbo_vtable = { vbo_map, vbo_unmap, vbo_bind, vbo_finished, vbo_destroy };

static vertex_buffer_t vbo_gl_create( uint32_t size )
{
    vertex_buffer_t buf = g_malloc(sizeof(struct vertex_buffer));
    memcpy( buf, &vbo_vtable, sizeof(struct vertex_buffer));

    glGenBuffers( 1, &buf->id );
    return buf;
}


/***************************** Vertex bindings *********************************/
static void bindings_gl_init( vertex_bindings_t map, vertex_buffer_t buf, vertex_attribute_t* attributes, int numAttribs )
{
    glGenVertexArrays(1, &map->id);
    glBindVertexArray(map->id);
    buf->bind( buf );
   
    for (int i = 0; i < numAttribs; i++)
    {
         vertex_attribute_t* a = &attributes[i];
         glEnableVertexAttribArray(a->attribLoc);
         glVertexAttribPointer(a->attribLoc,
                               a->components,
                               GL_FLOAT,
                               GL_FALSE,
                               a->stride,
                               (void *) (intptr_t) a->offset);
         a++;
    }
    glBindVertexArray(0);
}

static void bindings_gl_bind( vertex_bindings_t map )
{
    glBindVertexArray(map->id);
}

static void bindings_gl_finished( vertex_bindings_t map )
{
    glBindVertexArray(0);
}

static void bindings_gl_destroy( vertex_bindings_t map )
{
    glBindVertexArray(0);
    glDeleteVertexArrays(1, &map->id);
}

static struct vertex_bindings bindings_gl_vtable = { bindings_gl_init, bindings_gl_bind, bindings_gl_finished, bindings_gl_destroy };

static vertex_bindings_t bindings_gl_create( )
{
    vertex_bindings_t buf = g_malloc(sizeof(struct vertex_bindings));
    memcpy( buf, &bindings_gl_vtable, sizeof(struct vertex_bindings));
    return buf;
}


#ifdef HAVE_GLES2
/* Note: OpenGL ES only officialy supports glReadPixels for the RGBA32 format
 * (and doesn't necessarily support glPixelStore(GL_PACK_ROW_LENGTH) either.
 * As a result, we end up needed to do the format conversion ourselves.
 */

/**
 * Swizzle 32-bit RGBA to specified target format, truncating components where
 * necessary. Target may == source.
 * @param target destination buffer
 * @param Source buffer, which must be in 8-bit-per-component RGBA format, fully packed.
 * @param width width of image in pixels
 * @param height height of image in pixels
 * @param target_stride Stride of target buffer, in bytes
 * @param colour_format
 */
static void rgba32_to_target( unsigned char *target, const uint32_t *source, int width, int height, int target_stride, int colour_format )
{
    int x,y;

    if( target_stride == 0 )
        target_stride = width * colour_formats[colour_format].bpp;

    switch( colour_format ) {
    case COLFMT_BGRA1555:
        for( y=0; y<height; y++ ) {
            uint16_t *d = (uint16_t *)target;
            for( x=0; x<width; x++ ) {
                uint32_t v = *source++;
                *d++ = (uint16_t)( ((v & 0x80000000) >> 16) | ((v & 0x00F80000) >> 19) |
                        ((v & 0x0000F800)>>6) | ((v & 0x000000F8) << 7) );
            }
            target += target_stride;
        }
        break;
    case COLFMT_RGB565:
        for( y=0; y<height; y++ ) {
            uint16_t *d = (uint16_t *)target;
            for( x=0; x<width; x++ ) {
                uint32_t v = *source++;
                *d++ = (uint16_t)( ((v & 0x00F80000) >> 19) | ((v & 0x0000FC00) >> 5) | ((v & 0x000000F8)<<8) );
            }
            target += target_stride;
        }
        break;
    case COLFMT_BGRA4444:
        for( y=0; y<height; y++ ) {
            uint16_t *d = (uint16_t *)target;
            for( x=0; x<width; x++ ) {
                uint32_t v = *source++;
                *d++ = (uint16_t)( ((v & 0xF0000000) >> 16) | ((v & 0x00F00000) >> 20) |
                        ((v & 0x0000F000) >> 8) | ((v & 0x000000F0)<<4) );
            }
            target += target_stride;
        }
        break;
    case COLFMT_BGRA8888:
        for( y=0; y<height; y++ ) {
            uint32_t *d = (uint32_t *)target;
            for( x=0; x<width; x++ ) {
                uint32_t v = *source++;
                *d++ = (v & 0xFF00FF00) | ((v & 0x00FF0000) >> 16) | ((v & 0x000000FF)<<16);
            }
            target += target_stride;
        }
        break;
    case COLFMT_BGR0888:
        for( y=0; y<height; y++ ) {
            uint32_t *d = (uint32_t *)target;
            for( x=0; x<width; x++ ) {
                uint32_t v = *source++;
                *d++ = ((v & 0x00FF0000) >> 16) | (v & 0x0000FF00) | ((v & 0x000000FF)<<16);
            }
            target += target_stride;
        }
        break;
    case COLFMT_BGR888:
        for( y=0; y<height; y++ ) {
            uint8_t *d = (uint8_t *)target;
            for( x=0; x<width; x++ ) {
                uint32_t v = *source++;
                *d++ = (uint8_t)(v >> 16);
                *d++ = (uint8_t)(v >> 8);
                *d++ = (uint8_t)(v);
            }
            target += target_stride;
        }
        break;
    case COLFMT_RGB888:
        for( y=0; y<height; y++ ) {
            uint8_t *d = (uint8_t *)target;
            for( x=0; x<width; x++ ) {
                uint32_t v = *source++;
                *d++ = (uint8_t)(v);
                *d++ = (uint8_t)(v >> 8);
                *d++ = (uint8_t)(v >> 16);
            }
            target += target_stride;
        }
        break;
    default:
        assert( 0 && "Unsupported colour format" );
    }
}

/**
 * Convert data into an acceptable form for loading into an RGBA texture.
 */
static int target_to_rgba(  uint32_t *target, const unsigned char *source, int width, int height, int source_stride, int colour_format )
{
    int x,y;
    uint16_t *d;
    switch( colour_format ) {
    case COLFMT_BGRA1555:
        d = (uint16_t *)target;
        for( y=0; y<height; y++ ) {
            uint16_t *s = (uint16_t *)source;
            for( x=0; x<width; x++ ) {
                uint16_t v = *s++;
                *d++ = (v >> 15) | (v<<1);
            }
            source += source_stride;
        }
        return GL_UNSIGNED_SHORT_5_5_5_1;
        break;
    case COLFMT_RGB565:
        /* Need to expand to RGBA32 in order to have room for an alpha component */
        for( y=0; y<height; y++ ) {
            uint16_t *s = (uint16_t *)source;
            for( x=0; x<width; x++ ) {
                uint32_t v = (uint32_t)*s++;
                *target++ = ((v & 0xF800)>>8) | ((v & 0x07E0) <<5) | ((v & 0x001F) << 19) |
                        ((v & 0xE000) >> 13) | ((v &0x0600) >> 1) | ((v & 0x001C) << 14);
            }
            source += source_stride;
        }
        return GL_UNSIGNED_BYTE;
    case COLFMT_BGRA4444:
        d = (uint16_t *)target;
        for( y=0; y<height; y++ ) {
            uint16_t *s = (uint16_t *)source;
            for( x=0; x<width; x++ ) {
                uint16_t v = *s++;
                *d++ = (v >> 12) | (v<<4);
            }
            source += source_stride;
        }
        return GL_UNSIGNED_SHORT_4_4_4_4;
    case COLFMT_RGB888:
        for( y=0; y<height; y++ ) {
            uint8_t *s = (uint8_t *)source;
            for( x=0; x<width; x++ ) {
                *target++ = s[0] | (s[1]<<8) | (s[2]<<16);
                s += 3;
            }
            source += source_stride;
        }
        return GL_UNSIGNED_BYTE;
    case COLFMT_BGRA8888:
        for( y=0; y<height; y++ ) {
            uint32_t *s = (uint32_t *)source;
            for( x=0; x<width; x++ ) {
                uint32_t v = (uint32_t)*s++;
                *target++ = (v & 0xFF00FF00) | ((v & 0x00FF0000) >> 16) | ((v & 0x000000FF) << 16);
            }
            source += source_stride;
        }
        return GL_UNSIGNED_BYTE;
    case COLFMT_BGR0888:
        for( y=0; y<height; y++ ) {
            uint32_t *s = (uint32_t *)source;
            for( x=0; x<width; x++ ) {
                uint32_t v = (uint32_t)*s++;
                *target++ = (v & 0x0000FF00) | ((v & 0x00FF0000) >> 16) | ((v & 0x000000FF) << 16);
            }
            source += source_stride;
        }
        return GL_UNSIGNED_BYTE;
    case COLFMT_BGR888:
        for( y=0; y<height; y++ ) {
            uint8_t *s = (uint8_t *)source;
            for( x=0; x<width; x++ ) {
                *target++ = s[2] | (s[1]<<8) | (s[0]<<16);
                s += 3;
            }
            source += source_stride;
        }
        return GL_UNSIGNED_BYTE;
    default:
        assert( 0 && "Unsupported colour format" );
    }


}


gboolean gl_read_render_buffer( unsigned char *target, render_buffer_t buffer,
        int rowstride, int colour_format )
{
    if( colour_formats[colour_format].bpp == 4 && (rowstride == 0 || rowstride == buffer->width*4) ) {
        glReadPixels( 0, 0, buffer->width, buffer->height, GL_RGBA, GL_UNSIGNED_BYTE, target );
        rgba32_to_target( target, (uint32_t *)target, buffer->width, buffer->height, rowstride, colour_format );
    } else {
        int size = buffer->width * buffer->height;
        uint32_t tmp[size];

        glReadPixels( 0, 0, buffer->width, buffer->height, GL_RGBA, GL_UNSIGNED_BYTE, tmp );
        rgba32_to_target( target, tmp, buffer->width, buffer->height, rowstride, colour_format );
    }
    return TRUE;
}

void gl_pixel_buffer_to_tex( pixel_buffer_t frame, int tex_id )
{
    int size = frame->width * frame->height;
    uint32_t tmp[size];

    GLenum type = target_to_rgba( tmp, frame->data, frame->width, frame->height, frame->rowstride, frame->colour_format );
    glBindTexture( GL_TEXTURE_2D, tex_id );
    glTexSubImage2D( GL_TEXTURE_2D, 0, 0,0, frame->width, frame->height, GL_RGBA, type, tmp );
    gl_check_error("gl_pixel_buffer_to_tex:glTexSubImage2DBGRA");
}

#else
/**
 * Generic GL read_render_buffer. This function assumes that the caller
 * has already set the appropriate glReadBuffer(); in other words, unless
 * there's only one buffer this needs to be wrapped.
 */
gboolean gl_read_render_buffer( unsigned char *target, render_buffer_t buffer, 
                                int rowstride, int colour_format ) 
{
    glFinish();
    GLenum type = colour_formats[colour_format].type;
    GLenum format = colour_formats[colour_format].format;
    // int line_size = buffer->width * colour_formats[colour_format].bpp;
    // int size = line_size * buffer->height;
    int glrowstride = (rowstride / colour_formats[colour_format].bpp) - buffer->width;
    glPixelStorei( GL_PACK_ROW_LENGTH, glrowstride );
    glReadPixels( 0, 0, buffer->width, buffer->height, format, type, target );
    glPixelStorei( GL_PACK_ROW_LENGTH, 0 );
    return TRUE;
}

void gl_pixel_buffer_to_tex( pixel_buffer_t frame, int tex_id )
{
    GLenum type = colour_formats[frame->colour_format].type;
    GLenum format = colour_formats[frame->colour_format].format;
    int bpp = colour_formats[frame->colour_format].bpp;
    int rowstride = (frame->rowstride / bpp) - frame->width;

    glPixelStorei( GL_UNPACK_ROW_LENGTH, rowstride );
    glBindTexture( GL_TEXTURE_2D, tex_id );
    glTexSubImage2DBGRA( 0, 0,0,
                     frame->width, frame->height, format, type, frame->data, FALSE );
    glPixelStorei( GL_UNPACK_ROW_LENGTH, 0 );
}
#endif

void gl_load_pixel_buffer( pixel_buffer_t frame, render_buffer_t render )
{
    gl_pixel_buffer_to_tex( frame, render->tex_id );
}


/***************************** Driver implementation *********************************/
/**
 * Minimal GL driver (assuming that the GL context is already set up externally)
 * This requires FBO support (since otherwise we have no way to get a render buffer)
 */
#define DEFAULT_TERMINAL_COLUMNS 80
#define DEFAULT_COLUMN_WIDTH 34

static int compare_charp( const void *a, const void *b )
{
    const char **ca = (const char **)a;
    const char **cb = (const char **)b;
    return strcmp(*ca, *cb);
}

/**
 * Format a GL extension list (or other space-separated string) nicely, and
 * print to the given output stream.
 */
static void fprint_extensions( FILE *out, const char *extensions )
{
    unsigned int i, j, count, maxlen = DEFAULT_COLUMN_WIDTH;
    unsigned columns, per_column, terminal_columns;
    const char *terminal_columns_str = getenv("COLUMNS");

    if( terminal_columns_str == NULL || (terminal_columns = strtol(terminal_columns_str,0,10)) == 0 )
        terminal_columns = DEFAULT_TERMINAL_COLUMNS;

    if( extensions == NULL || extensions[0] == '\0' )
        return;

    gchar *ext_dup = g_strdup(extensions);
    gchar **ext_split = g_strsplit(g_strstrip(ext_dup), " ", 0);
    for( count = 0; ext_split[count] != NULL; count++ ) {
        unsigned len = strlen(ext_split[count]);
        if( len > maxlen )
            maxlen = len;
    }

    columns = terminal_columns / (maxlen+2);
    if( columns == 0 )
        columns = 1;
    per_column = (count+columns-1) / columns;

    qsort(ext_split, count, sizeof(gchar *), compare_charp);

    for( i=0; i<per_column; i++ ) {
        for( j=0; j<columns; j++ ) {
            unsigned idx = i + (j*per_column);
            if( idx < count )
                fprintf( out, "  %-*s", maxlen, ext_split[idx] );
        }
        fprintf( out, "\n" );
    }
    g_strfreev(ext_split);
    g_free(ext_dup);
}

static void render_gl_print_info( FILE *out )
{
    fprintf( out, "GL Vendor: %s\n",   glGetString(GL_VENDOR) );
    fprintf( out, "GL Renderer: %s\n", glGetString(GL_RENDERER) );
    fprintf( out, "GL Version: %s\n",  glGetString(GL_VERSION) );
    if( isGLShaderSupported() ) {
         fprintf( out, "SL Version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION) );
    }

    fprintf( out, "GL Extensions:\n" );
    fprint_extensions( out, (const gchar *)glGetString(GL_EXTENSIONS) );
}

struct render_driver render_gl_driver = { 
        NULL,//fbo_gl_create,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        vbo_gl_create,
        bindings_gl_create,
        render_gl_print_info
};

gboolean gl_init_render( display_driver_t driver )
{
    render_driver_t render = &render_gl_driver;
    render_driver = render;

    render->print_info = render_gl_print_info;
    gl_fbo_init(render);

    /* Use SL shaders if available */
    gboolean result = glsl_init(render);

    if (!result) { /* Shaders are required */
        gl_fbo_shutdown();
        return FALSE;
    }

    render->capabilities.has_gl = TRUE;
    render->capabilities.has_bgra = isGLBGRATextureSupported();
    return TRUE;
}
