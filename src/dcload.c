/**
 * $Id$
 *
 * DC-load syscall implementation.
 *
 * Copyright (c) 2005 Nathan Keynes.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <arpa/inet.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>

#include "dream.h"
#include "mem.h"
#include "dreamcast.h"
#include "syscall.h"
#include "sh4/sh4.h"

#define SYS_READ 0
#define SYS_WRITE 1
#define SYS_OPEN 2
#define SYS_CLOSE 3
#define SYS_CREAT 4
#define SYS_LINK 5
#define SYS_UNLINK 6
#define SYS_CHDIR 7
#define SYS_CHMOD 8
#define SYS_LSEEK 9
#define SYS_FSTAT 10
#define SYS_TIME 11
#define SYS_STAT 12
#define SYS_UTIME 13
#define SYS_ASSIGNWRKMEM 14
#define SYS_EXIT 15
#define SYS_OPENDIR 16
#define SYS_CLOSEDIR 17
#define SYS_READDIR 18
#define SYS_GETHOSTINFO 19
#define SYS_GDBPACKET 20
#define SYS_REWINDDIR 21

#define SYS_MAGIC      0xDEADBEEF
#define SYS_MAGIC_ADDR 0x8c004004
#define SYSCALL_ADDR   0x8c004008
static int dcload_workmem_addr;

typedef struct dcload_dirent {
    int32_t  d_ino;       /* inode number */
    uint32_t d_off;       /* offset to the next dirent */
    uint16_t d_reclen;    /* length of this record */
    uint8_t  d_type;      /* type of file */
    char     d_name[256]; /* filename */
} dcload_dirent_t;

typedef struct dcload_stat {
    uint16_t st_dev;
    uint16_t st_ino;
    int32_t  st_mode;
    uint16_t st_nlink;
    uint16_t st_uid;
    uint16_t st_gid;
    uint16_t st_rdev;
    int32_t  st_size;
    int32_t  atime;
    int32_t  st_spare1;
    int32_t  mtime;
    int32_t  st_spare2;
    int32_t  ctime;
    int32_t  st_spare3;
    int32_t  st_blksize;
    int32_t  st_blocks;
    int32_t  st_spare4[2];
} dcload_stat_t;

static unsigned int dc_order(unsigned int x) {
    if (x == htonl(x))
    return (x << 24) | ((x << 8) & 0xff0000) | ((x >> 8) & 0xff00) | ((x >> 24) & 0xff);
    else
    return x;
}


static gboolean dcload_allow_unsafe = FALSE;

void dcload_set_allow_unsafe( gboolean allow )
{
    dcload_allow_unsafe = allow;
}

static char* dcload_chroot_dir = NULL;

void dcload_set_chroot_dir( char* mount_dir ) {
    dcload_chroot_dir = mount_dir;
}


static const char* apply_chroot(const char* filename) {
    static char buffer[4096];

    /* FIXME: Prevent escaping from the specified directory
     * (e.g. if the filename is "../../" etc.) */
    snprintf(
        buffer,
        4096,
        "%s%s",
        (dcload_chroot_dir) ? dcload_chroot_dir : "", filename
    );

    return buffer;
}


#define MAX_OPEN_FDS 16
/**
 * Mapping from emulator fd to real fd (so we can limit read/write
 * to only fds we've explicitly granted access to).
 */
static int open_file_fds[MAX_OPEN_FDS];
static DIR* open_dir_fds[MAX_OPEN_FDS];

static int dcload_alloc_file_fd()
{
    int i;
    for( i=0; i<MAX_OPEN_FDS; i++ ) {
        if( open_file_fds[i] == -1 ) return i;
    }
    return -1;
}

static int dcload_alloc_dir_fd()
{
    int i;
    for( i=0; i<MAX_OPEN_FDS; i++ ) {
        if( open_dir_fds[i] == NULL ) return i;
    }
    return -1;
}


static int dcload_syscall_read(void) 
{
    int fd = sh4r.r[5];
    if( fd < 0 || fd >= MAX_OPEN_FDS || open_file_fds[fd] == -1 ) {
        return -1;
    }
    
    sh4ptr_t buf = mem_get_region( sh4r.r[6] );
    int length = sh4r.r[7];
    return read( open_file_fds[fd], buf, length );
}

static int dcload_syscall_write(void) 
{
    int fd = sh4r.r[5];
    if( fd < 0 || fd >= MAX_OPEN_FDS || open_file_fds[fd] == -1 ) {
        return -1;
    }
    
    sh4ptr_t buf = mem_get_region( sh4r.r[6] );
    int length = sh4r.r[7];
    return write( open_file_fds[fd], buf, length );
}

static int dcload_syscall_seek(void) 
{
    int fd = sh4r.r[5];
    if( fd < 0 || fd >= MAX_OPEN_FDS || open_file_fds[fd] == -1 ) {
        return -1;
    }

    return lseek( open_file_fds[fd], (int32_t)sh4r.r[6], sh4r.r[7] );
}

static int dcload_syscall_open(void) 
{
    int fd = dcload_alloc_file_fd();
    if( fd == -1 ) return -1;

    char *filename = (char *)mem_get_region( sh4r.r[5] );
    int realfd = open( apply_chroot(filename), sh4r.r[6] );
    open_file_fds[fd] = realfd;

    if( realfd == -1 ) return -1;
    return fd;
}

static int dcload_syscall_close(void) 
{
    int fd = sh4r.r[5];
    if( fd < 0 || fd >= MAX_OPEN_FDS || open_file_fds[fd] == -1 ) {
        return -1;
    }

    /* Don't actually close real fds 0-2 */
    if( open_file_fds[fd] <= 2 ) return 0;

    int ret = close( open_file_fds[fd] );
    open_file_fds[fd] = -1;
    return ret;
}

static int dcload_syscall_stat(void) 
{
    char *filename = (char *)mem_get_region( sh4r.r[5] );
    sh4ptr_t ptr = mem_get_region( sh4r.r[6] );
    dcload_stat_t *stat_out = (dcload_stat_t *) ptr;

    struct stat s;

    const char* final_filename = apply_chroot(filename);
    int err = stat(final_filename, &s);
    if(err == 0 && stat_out) {
        /* On success, copy the output. This are the only
         * fields copied by dcload at the time of writing */

        stat_out->st_dev = dc_order(s.st_dev);
        stat_out->st_ino = dc_order(s.st_ino) & 0xFFFF;
        stat_out->st_mode = dc_order(s.st_mode);
        stat_out->st_nlink = dc_order(s.st_nlink);
        stat_out->st_uid = dc_order(s.st_uid);
        stat_out->st_gid = dc_order(s.st_gid);
        stat_out->st_rdev = dc_order(s.st_rdev);
        stat_out->st_size = dc_order(s.st_size);
        stat_out->st_blksize = dc_order(s.st_blksize);
        stat_out->st_blocks = dc_order(s.st_blocks);
    }
    return err;
}


static int dcload_syscall_opendir(void) 
{
    int fd = dcload_alloc_dir_fd();
    if( fd == -1 ) return 0;

    char *filename   = (char *)mem_get_region( sh4r.r[5] );
    DIR* host_dir    = opendir( apply_chroot(filename) );
    open_dir_fds[fd] = host_dir;

    if( host_dir == NULL ) return 0;
    /* KallistiOS treats values below 100 as regular file FDs */
    return fd + 10000;
}

static int dcload_syscall_closedir(void) 
{
    int fd = sh4r.r[5] - 10000;
    if( fd < 0 || fd >= MAX_OPEN_FDS || open_dir_fds[fd] == NULL ) {
        return -1;
    }

    int ret = closedir( open_dir_fds[fd] );
    open_dir_fds[fd] = NULL;
    return ret;
}

static int dcload_syscall_readdir(void) 
{
    int fd = sh4r.r[5] - 10000;
    if( fd < 0 || fd >= MAX_OPEN_FDS || open_dir_fds[fd] == NULL ) {
        return 0;
    }

    struct dirent* ent = readdir( open_dir_fds[fd] );
    if( ent == NULL ) return 0;

    /* have to return an address in memory */
    if( dcload_workmem_addr == 0 ) return 0;
    sh4ptr_t ptr = mem_get_region( dcload_workmem_addr );

    dcload_dirent_t *dst = (dcload_dirent_t*)ptr;
    dst->d_ino    = dc_order( ent->d_ino );
    dst->d_off    = 0;
    dst->d_reclen = dc_order( 11 + strlen(ent->d_name) );
    dst->d_type   = ent->d_type;
    strncpy( dst->d_name, ent->d_name, sizeof(dst->d_name) );
    dst->d_name[sizeof(dst->d_name) - 1] = '\0';

    return dcload_workmem_addr;
}

static int dcload_syscall_rewinddir(void) 
{
    int fd = sh4r.r[5] - 10000;
    if( fd < 0 || fd >= MAX_OPEN_FDS || open_dir_fds[fd] == NULL ) {
        return -1;
    }

    rewinddir( open_dir_fds[fd] );
    return 0;
}

static int dcload_syscall_setworkmem(void) 
{
    dcload_workmem_addr = sh4r.r[5];
    /* This forces dc-load to identify as serial */
    return 1;
}

void dcload_syscall( uint32_t syscall_id )
{
    uint32_t syscall = sh4r.r[4];

    switch( syscall ) {
    case SYS_READ:
        sh4r.r[0] = dcload_syscall_read();
        break;
    case SYS_WRITE:
        sh4r.r[0] = dcload_syscall_write();
        break;
    case SYS_LSEEK:
        sh4r.r[0] = dcload_syscall_seek();
        break;

        /* Secure access only */
    case SYS_OPEN:
        if( dcload_allow_unsafe ) {
            sh4r.r[0] = dcload_syscall_open();
        } else {
            ERROR( "Denying access to local filesystem" );
            sh4r.r[0] = -1;
        }
        break;
    case SYS_CLOSE:
        sh4r.r[0] = dcload_syscall_close();
        break;
    case SYS_STAT:
        if( dcload_allow_unsafe ) {
            sh4r.r[0] = dcload_syscall_stat();
        } else {
            ERROR( "Denying access to local filesystem" );
            sh4r.r[0] = -1;
        }
        break;

    case SYS_OPENDIR:
        if( dcload_allow_unsafe ) {
            sh4r.r[0] = dcload_syscall_opendir();
        } else {
            ERROR( "Denying access to local filesystem" );
            sh4r.r[0] = -1;
        }
        break;
    case SYS_CLOSEDIR:
        sh4r.r[0] = dcload_syscall_closedir();
        break;
    case SYS_READDIR:
        sh4r.r[0] = dcload_syscall_readdir();
        break;
    case SYS_REWINDDIR:
        sh4r.r[0] = dcload_syscall_rewinddir();
        break;

    case SYS_ASSIGNWRKMEM:
        sh4r.r[0] = dcload_syscall_setworkmem();
        break;
    case SYS_EXIT:
        if( dcload_allow_unsafe ) {
            dreamcast_shutdown();
            exit( sh4r.r[5] );
        } else {
            dreamcast_stop();
        }


    default:
        WARN("Unknown Syscall: %d\n", syscall);
        sh4r.r[0] = -1;
    }

}

void dcload_install()
{
    dcload_workmem_addr = 0;
    memset( &open_file_fds, -1, sizeof(open_file_fds) );
    memset( &open_dir_fds,   0, sizeof(open_dir_fds)  );

    open_file_fds[0] = 0; /* STDIN  */
    open_file_fds[1] = 1; /* STDOUT */
    open_file_fds[2] = 2; /* STDERR */
    syscall_add_hook_vector( 0xF0, SYSCALL_ADDR, dcload_syscall );
    mem_write_long( SYS_MAGIC_ADDR, SYS_MAGIC );
}
