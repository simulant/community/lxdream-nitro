/**
 * $Id$
 *
 * Implements the MMIO register viewing window
 *
 * Copyright (c) 2005 Nathan Keynes.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <stdint.h>
#include <string.h>
#include "gtkui/gtkui.h"
#include "mem.h"
#include "mmio.h"


struct mmio_window_info {
    GtkWidget *window;
    GtkWidget *notebook;
};

static void printbits( char *out, int nbits, uint32_t value )
{
    if( nbits < 32 ) {
        int i;
        for( i=32; i>nbits; i-- ) {
            if( !(i % 8) ) *out++ = ' ';
            *out++ = ' ';
        }
    }
    while( nbits > 0 ) {
        *out++ = (value&(1<<--nbits) ? '1' : '0');
        if( !(nbits % 8) ) *out++ = ' ';
    }
    *out = '\0';
}

static void printhex( char *out, int nbits, uint32_t value )
{
    char tmp[10], *p = tmp;
    int i;

    sprintf( tmp, "%08X", value );
    for( i=32; i>0; i-=4, p++ ) {
        if( i <= nbits ) *out++ = *p;
        else *out++ = ' ';
    }
    *out = '\0';
}


/************************** MMIO register table rows *****************************/
enum MMIOPageColumns {
    PAGE_COLUMN_ADDRESS,
    PAGE_COLUMN_REGISTER,
    PAGE_COLUMN_VALUE,
    PAGE_COLUMN_BIT_PATTERN,
    PAGE_COLUMN_DESCRIPTION,
	PAGE_COLUMN_TEXTCOLOR,
    PAGE_COLUMN_COUNT
};

static void mmio_page_add_row( GtkTreeView *tree, int i, int j )
{
    GtkTreeModel *model = gtk_tree_view_get_model( tree );
    GtkListStore *list  = GTK_LIST_STORE( model );

    int sz = io_rgn[i]->ports[j].width;
    char addr[10], data[10], bits[40];
    GtkTreeIter iter;

    sprintf( addr, "%08X",
             io_rgn[i]->base + io_rgn[i]->ports[j].offset );
    printhex( data, sz,  *io_rgn[i]->ports[j].val );
    printbits( bits, sz, *io_rgn[i]->ports[j].val );

    gtk_list_store_append( list, &iter );
    gtk_list_store_set(
        list, &iter,
        PAGE_COLUMN_ADDRESS,     addr,
        PAGE_COLUMN_REGISTER,    io_rgn[i]->ports[j].id,
        PAGE_COLUMN_VALUE,       data,
        PAGE_COLUMN_BIT_PATTERN, bits,
        PAGE_COLUMN_DESCRIPTION, io_rgn[i]->ports[j].desc,
        PAGE_COLUMN_TEXTCOLOR,   NULL,
        -1
    );
}

static void mmio_page_update_row( GtkTreeView *tree, int idx, int i, int j )
{
    GtkTreeModel *model = gtk_tree_view_get_model( tree );
    GtkListStore *list  = GTK_LIST_STORE( model );

    GtkTreeIter iter;
    assert(gtk_tree_model_iter_nth_child( model, &iter, NULL, idx ));

    char data[10], bits[40];
    int sz = io_rgn[i]->ports[j].width;
    printhex(  data, sz, *io_rgn[i]->ports[j].val );
    printbits( bits, sz, *io_rgn[i]->ports[j].val );

    gtk_list_store_set(
        list, &iter,
        PAGE_COLUMN_VALUE,       data,
        PAGE_COLUMN_BIT_PATTERN, bits,
        PAGE_COLUMN_TEXTCOLOR,   &gui_colour_changed,
        -1
    );
}

static void mmio_page_reset_row( GtkTreeView *tree, int idx )
{
    GtkTreeModel *model = gtk_tree_view_get_model( tree );
    GtkListStore *list  = GTK_LIST_STORE( model );

    GtkTreeIter iter;
    assert(gtk_tree_model_iter_nth_child( model, &iter, NULL, idx ));

    gtk_list_store_set(
        list, &iter,
        PAGE_COLUMN_TEXTCOLOR, NULL,
        -1
    );
}


/*************************** MMIO register tables ******************************/
static void add_tree_view_column( GtkTreeView *list, GtkCellRenderer *renderer, const char* title, int column )
{
    gtk_tree_view_insert_column_with_attributes( list, column, title, renderer, 
                                                "text", column, "foreground-gdk", PAGE_COLUMN_TEXTCOLOR, NULL );
    GtkTreeViewColumn *col = gtk_tree_view_get_column( list, column );
    gtk_tree_view_column_set_resizable( col, TRUE );
}

static void on_trace_button_toggled( GtkToggleButton *button, gpointer user_data )
{
    struct mmio_region *io_rgn = (struct mmio_region *)user_data;
    gboolean isActive = gtk_toggle_button_get_active(button);
    if( io_rgn != NULL ) {
        io_rgn->trace_flag = isActive ? 1 : 0;
    }
}

static GtkTreeView *mmio_page_add( mmio_window_t mmio, char *name, struct mmio_region *io_rgn )
{
    GtkCellRenderer *renderer;
    GtkTreeView *list;
    GtkWidget *scroll;
    GtkWidget *tab;
    GtkCheckButton *trace_button;
    GtkListStore* store;
    GtkVBox *vbox;

    scroll = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy( GTK_SCROLLED_WINDOW(scroll),
                                    GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS );

    store = GTK_LIST_STORE(
        gtk_list_store_new(PAGE_COLUMN_COUNT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, GDK_TYPE_COLOR)
    );
    list = GTK_TREE_VIEW(gtk_tree_view_new_with_model(GTK_TREE_MODEL(store)));

    renderer = gtk_cell_renderer_text_new();
    gtk_cell_renderer_set_padding( renderer, 0, 0 );

    add_tree_view_column( list, renderer, "Address",     PAGE_COLUMN_ADDRESS );
    add_tree_view_column( list, renderer, "Register",    PAGE_COLUMN_REGISTER );
    add_tree_view_column( list, renderer, "Value",       PAGE_COLUMN_VALUE );
    add_tree_view_column( list, renderer, "Bit Pattern", PAGE_COLUMN_BIT_PATTERN );
    add_tree_view_column( list, renderer, "Description", PAGE_COLUMN_DESCRIPTION );

    gtk_widget_modify_font( GTK_WIDGET(list), gui_fixed_font );
    tab = gtk_label_new(_(name));
    gtk_container_add( GTK_CONTAINER(scroll), GTK_WIDGET(list) );

    vbox = GTK_VBOX(gtk_vbox_new( FALSE, 0 ));
    gtk_container_add( GTK_CONTAINER(vbox), GTK_WIDGET(scroll) );

    trace_button = GTK_CHECK_BUTTON(gtk_check_button_new_with_label(_("Trace access")));
    if( io_rgn != NULL ) {
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(trace_button), 
                io_rgn->trace_flag ? TRUE : FALSE);
    }
    gtk_container_add( GTK_CONTAINER(vbox), GTK_WIDGET(trace_button) );
    gtk_box_set_child_packing( GTK_BOX(vbox), GTK_WIDGET(trace_button), 
                               FALSE, FALSE, 0, GTK_PACK_START );
    gtk_notebook_append_page( GTK_NOTEBOOK(mmio->notebook), GTK_WIDGET(vbox), tab );
    g_object_set_data( G_OBJECT(mmio->window), name, list );
    g_signal_connect ((gpointer) trace_button, "toggled",
                      G_CALLBACK (on_trace_button_toggled),
                      io_rgn);
    return list;
}

static void mmio_pages_init( mmio_window_t mmio )
{
    GtkTreeView *all_list;
    GtkTreeView *list;
    int i, j;

    /* Add the mmio register data */
    all_list = mmio_page_add( mmio, "All", NULL );

    for( i=0; i < num_io_rgns; i++ ) {
        list = mmio_page_add( mmio, io_rgn[i]->id, io_rgn[i] );
        for( j=0; io_rgn[i]->ports[j].id != NULL; j++ ) {
            mmio_page_add_row( list,     i, j );
            mmio_page_add_row( all_list, i, j );
        }
    }
}

static void mmio_pages_update( mmio_window_t mmio )
{
    int i,j, count = 0;
    GtkTreeView *page, *all_page;

    all_page = GTK_TREE_VIEW(g_object_get_data( G_OBJECT(mmio->window), "All" ));

    for( i=0; i < num_io_rgns; i++ ) {
        page = GTK_TREE_VIEW(g_object_get_data( G_OBJECT(mmio->window),
                io_rgn[i]->id ));
        for( j=0; io_rgn[i]->ports[j].id != NULL; j++ ) {
            if( *io_rgn[i]->ports[j].val !=
                *(uint32_t *)(io_rgn[i]->save_mem+io_rgn[i]->ports[j].offset)){

                mmio_page_update_row( page,         j, i, j );
                mmio_page_update_row( all_page, count, i, j );

            } else {
                mmio_page_reset_row( page,         j );
                mmio_page_reset_row( all_page, count );
            }
            count++;
        }
        memcpy( io_rgn[i]->save_mem, io_rgn[i]->mem, LXDREAM_PAGE_SIZE );
    }
}


/*************************** Overall MMIO window ******************************/
static gboolean on_mmio_delete_event(GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
    gtk_widget_hide(widget);
    return TRUE;
}

static void on_mmio_close_clicked( GtkButton *button, gpointer user_data)
{
    gtk_widget_hide( ((mmio_window_t)user_data)->window );
}

mmio_window_t mmio_window_new( const gchar *title )
{
    mmio_window_t mmio = g_malloc0( sizeof(struct mmio_window_info) );
    GtkWidget *vbox1;
    GtkWidget *hbuttonbox1;
    GtkWidget *mmr_close;

    mmio->window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (mmio->window), title);
    gtk_window_set_default_size (GTK_WINDOW (mmio->window), 600, 600);

    vbox1 = gtk_vbox_new (FALSE, 0);
    gtk_container_add (GTK_CONTAINER (mmio->window), vbox1);

    mmio->notebook = gtk_notebook_new ();
    gtk_box_pack_start (GTK_BOX (vbox1), mmio->notebook, TRUE, TRUE, 0);
    gtk_notebook_set_tab_pos (GTK_NOTEBOOK (mmio->notebook), GTK_POS_LEFT);

    hbuttonbox1 = gtk_hbutton_box_new ();
    gtk_box_pack_start (GTK_BOX (vbox1), hbuttonbox1, FALSE, TRUE, 0);
    gtk_box_set_spacing (GTK_BOX (hbuttonbox1), 30);

    mmr_close = gtk_button_new_with_mnemonic (_("Close"));
    gtk_container_add (GTK_CONTAINER (hbuttonbox1), mmr_close);
    gtk_widget_set_can_default(mmr_close, TRUE);

    mmio_pages_init( mmio );

    g_signal_connect ((gpointer) mmio->window, "delete_event",
                      G_CALLBACK (on_mmio_delete_event),
                      NULL);
    g_signal_connect ((gpointer) mmr_close, "clicked",
                      G_CALLBACK (on_mmio_close_clicked),
                      mmio);

    gtk_widget_show_all( mmio->window );
    return mmio;
}

void mmio_window_update( mmio_window_t mmio )
{
    mmio_pages_update( mmio );
}

void mmio_window_show( mmio_window_t mmio, gboolean show )
{
    if( show ) {
        gtk_widget_show( mmio->window );
    } else {
        gtk_widget_hide( mmio->window );
    }
}

