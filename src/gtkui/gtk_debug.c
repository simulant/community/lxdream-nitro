/**
 * $Id$
 * This file is responsible for the main debugger gui frame.
 *
 * Copyright (c) 2005 Nathan Keynes.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <gdk/gdkkeysyms.h>
#include "mem.h"
#include "cpu.h"
#include "dreamcast.h"
#include "gtkui/gtkui.h"
#include "sh4/sh4.h"
#include "aica/armdasm.h"

GdkColor *msg_colors[] = { &gui_colour_error, &gui_colour_error, &gui_colour_warn, 
        &gui_colour_normal,&gui_colour_debug, &gui_colour_trace };

const cpu_desc_t cpu_list[4] = { &sh4_cpu_desc, &arm_cpu_desc, &armt_cpu_desc, NULL };

uint32_t row_to_address( debug_window_t data, int row );
int address_to_row( debug_window_t data, uint32_t address );
void set_disassembly_pc( debug_window_t data, unsigned int pc, gboolean select );
void set_disassembly_region( debug_window_t data, unsigned int page );
void set_disassembly_cpu( debug_window_t data, const gchar *cpu );
void jump_to_pc( debug_window_t data, gboolean select );

struct debug_window_info {
    int disasm_from;
    int disasm_to;
    int disasm_pc;
    const struct cpu_desc_struct *cpu;
    const cpu_desc_t *cpu_list;
    GtkTreeView *regs_list;
    GtkTreeView *disasm_list;
    GtkEntry *page_field;
    GtkWidget *window;
    GtkWidget *statusbar;
    char saved_regs[0];
};


/************************** Disassembly panel controls *****************************/
static void disasm_controls_page_set_addr( debug_window_t data, unsigned int mem_addr )
{
    char addr[10];
    sprintf( addr, "%08X", mem_addr );
    gtk_entry_set_text( data->page_field, addr );
}

static gboolean on_page_field_key_press_event( GtkWidget * widget, GdkEventKey *event, gpointer user_data)
{
    debug_window_t data = (debug_window_t)user_data;

    if( event->keyval == GDK_KEY_Return || event->keyval == GDK_KEY_Linefeed ) {
        const gchar *text = gtk_entry_get_text( GTK_ENTRY(widget) );
        gchar *endptr;
        unsigned int val = strtoul( text, &endptr, 16 );

        if( text == endptr ) { /* invalid input */
            disasm_controls_page_set_addr( data, row_to_address(data,0) );
        } else {
            set_disassembly_region(data, val);
        }
    }
    return FALSE;
}

static void on_jump_pc_btn_clicked( GtkButton *button, gpointer user_data)
{
    debug_window_t data = (debug_window_t)user_data;
    jump_to_pc( data, TRUE );
}

static void on_mode_box_changed( GtkComboBoxText *combo_box, gpointer user_data)
{
    const gchar *text = gtk_combo_box_text_get_active_text( combo_box );
    set_disassembly_cpu( gtk_gui_get_debugger(), text );
}

static void disasm_controls_add( debug_window_t data, GtkWidget *disasm_box )
{
    GtkWidget *hbox1 = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (disasm_box), hbox1, FALSE, FALSE, 3);
    gtk_box_pack_start (GTK_BOX (hbox1), gtk_label_new (_("Page")), FALSE, FALSE, 4);

    data->page_field = GTK_ENTRY(gtk_entry_new ());
    gtk_box_pack_start (GTK_BOX (hbox1), GTK_WIDGET(data->page_field), FALSE, TRUE, 0);

    GtkWidget *jump_pc_btn = gtk_button_new_with_mnemonic (_(" Jump to PC "));
    gtk_box_pack_start (GTK_BOX (hbox1), jump_pc_btn, FALSE, FALSE, 4);

    gtk_box_pack_start (GTK_BOX (hbox1), gtk_label_new(_("Mode")), FALSE, FALSE, 5);

    GtkWidget *mode_box = gtk_combo_box_text_new();
    gtk_box_pack_start (GTK_BOX (hbox1), mode_box, FALSE, FALSE, 0);

    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(mode_box), _("SH4"));
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(mode_box), _("ARM7"));
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(mode_box), _("ARM7T"));
    gtk_combo_box_set_active(GTK_COMBO_BOX(mode_box), 0);


    g_signal_connect ((gpointer) data->page_field, "key_press_event",
                      G_CALLBACK (on_page_field_key_press_event),
                      data);
    g_signal_connect ((gpointer) jump_pc_btn, "clicked",
                      G_CALLBACK (on_jump_pc_btn_clicked),
                      data);
    g_signal_connect ((gpointer) mode_box, "changed",
                      G_CALLBACK (on_mode_box_changed),
                      data);
}

/************************** Disassembly table rows *****************************/
enum DisasmColumns {
    DISASM_COLUMN_ADDRESS,
    DISASM_COLUMN_UNKNOWN,
    DISASM_COLUMN_OPCODE,
    DISASM_COLUMN_BUFFER,
    DISASM_COLUMN_FORECOLOR,
    DISASM_COLUMN_BACKCOLOR,
    DISASM_COLUMN_COUNT
};

static void disasm_table_forecolor_row( GtkTreeView *tree, int row, GdkColor* color )
{
    GtkTreeModel *model = gtk_tree_view_get_model( tree );
    GtkListStore *list  = GTK_LIST_STORE( model );

    GtkTreeIter iter;
    assert(gtk_tree_model_iter_nth_child( model, &iter, NULL, row ));

    gtk_list_store_set(
        list, &iter,
        DISASM_COLUMN_FORECOLOR, color,
        -1
    );
}

static void disasm_table_backcolor_row( GtkTreeView *tree, int row, GdkColor* color )
{
    GtkTreeModel *model = gtk_tree_view_get_model( tree );
    GtkListStore *list  = GTK_LIST_STORE( model );

    GtkTreeIter iter;
    assert(gtk_tree_model_iter_nth_child( model, &iter, NULL, row ));

    gtk_list_store_set(
        list, &iter,
        DISASM_COLUMN_BACKCOLOR, color,
        -1
    );
}

static void disasm_table_add_row( GtkTreeView *tree, char** args )
{
    GtkTreeModel *model = gtk_tree_view_get_model( tree );
    GtkListStore *list  = GTK_LIST_STORE( model );
    GtkTreeIter iter;

    gtk_list_store_append( list, &iter );
    gtk_list_store_set(
        list, &iter,
        DISASM_COLUMN_ADDRESS, args[0],
        DISASM_COLUMN_UNKNOWN, args[1],
        DISASM_COLUMN_OPCODE,  args[2],
        DISASM_COLUMN_BUFFER,  args[3],
        -1
    );
}

static void disasm_table_add_unmapped_row( GtkTreeView *tree, unsigned int mem_addr )
{
    GtkTreeModel *model = gtk_tree_view_get_model( tree );
    GtkListStore *list  = GTK_LIST_STORE( model );
    GtkTreeIter iter;

    char addr[10];
    sprintf( addr, "%08X", mem_addr );

    gtk_list_store_append( list, &iter );
    gtk_list_store_set(
        list, &iter,
        DISASM_COLUMN_ADDRESS, addr,
        DISASM_COLUMN_UNKNOWN, " ",
        DISASM_COLUMN_OPCODE,  "",
        DISASM_COLUMN_BUFFER,  _("This page is currently unmapped"),
        DISASM_COLUMN_FORECOLOR, &gui_colour_error,
        -1
    );
}

static void disasm_table_clear_rows( GtkTreeView *tree )
{
    GtkTreeModel *model = gtk_tree_view_get_model( tree );
    GtkListStore *list  = GTK_LIST_STORE( model );

    gtk_list_store_clear( list );
}


/************************** Disassembly table *****************************/
static void on_disasm_list_selection_changed( GtkTreeSelection* sel, gpointer user_data )
{
    GtkTreeModel *model;
    GtkTreeIter iter;

    if( gtk_tree_selection_get_selected( sel, &model, &iter) ) {
        gtk_gui_enable_action( "SetBreakpoint", TRUE );
        gtk_gui_enable_action( "RunTo", dreamcast_can_run() );
    } else {
        gtk_gui_enable_action( "SetBreakpoint", FALSE );
        gtk_gui_enable_action( "RunTo", FALSE );
    }
}

static int disasm_table_get_selected_row( debug_window_t data )
{
    GtkTreeSelection* sel = gtk_tree_view_get_selection( data->disasm_list );
    GtkTreeModel *model;
    GtkTreePath *path;
    GtkTreeIter iter;
    int *i;

    if( !gtk_tree_selection_get_selected( sel, &model, &iter ) )
        return -1;

    path = gtk_tree_model_get_path( model, &iter );
    i = gtk_tree_path_get_indices( path );
    return i[0];
}

static void disasm_table_add_column( GtkTreeView *list, GtkCellRenderer *renderer, const char* title, int column, int width )
{
    gtk_tree_view_insert_column_with_attributes( list, column, title, renderer, 
                                                "text", column, 
                                                "foreground-gdk",      DISASM_COLUMN_FORECOLOR, 
                                                "cell-background-gdk", DISASM_COLUMN_BACKCOLOR, 
                                                NULL );
    GtkTreeViewColumn *col = gtk_tree_view_get_column( list, column );
    gtk_tree_view_column_set_sizing(    col, GTK_TREE_VIEW_COLUMN_FIXED );
    gtk_tree_view_column_set_min_width( col, width );
}

static void disasm_table_add( debug_window_t data, GtkWidget *disasm_box )
{
    GtkCellRenderer *renderer;
    GtkListStore* store;

    GtkWidget *disasm_scroll = gtk_scrolled_window_new (NULL, NULL);
    gtk_box_pack_start (GTK_BOX (disasm_box), disasm_scroll, TRUE, TRUE, 0);
    gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (disasm_scroll), GTK_SHADOW_IN);

    store = GTK_LIST_STORE(
        gtk_list_store_new(DISASM_COLUMN_COUNT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, GDK_TYPE_COLOR, GDK_TYPE_COLOR)
    );
    data->disasm_list = GTK_TREE_VIEW(gtk_tree_view_new_with_model(GTK_TREE_MODEL(store)));

    renderer = gtk_cell_renderer_text_new();
    gtk_cell_renderer_set_padding( renderer, 0, 0 );
	gtk_tree_view_set_headers_visible( data->disasm_list, FALSE );

    disasm_table_add_column( data->disasm_list, renderer, _("Address"), DISASM_COLUMN_ADDRESS, 80 );
    disasm_table_add_column( data->disasm_list, renderer, _("Unknown"), DISASM_COLUMN_UNKNOWN, 16 );
    disasm_table_add_column( data->disasm_list, renderer, _("Opcode"),  DISASM_COLUMN_OPCODE,  80 );
    disasm_table_add_column( data->disasm_list, renderer, _("Buffer"),  DISASM_COLUMN_BUFFER,  80 );

    gtk_container_add (GTK_CONTAINER (disasm_scroll), GTK_WIDGET(data->disasm_list));

    GtkTreeSelection* sel = gtk_tree_view_get_selection( data->disasm_list );
    g_signal_connect ((gpointer) sel, "changed",
                      G_CALLBACK (on_disasm_list_selection_changed),
                      data);
}

static void disasm_table_select_row( GtkTreeView *tree, int row )
{
    GtkTreePath *path = gtk_tree_path_new_from_indices( row, -1 );
    gtk_tree_view_set_cursor( tree, path, NULL, FALSE );
    gtk_tree_path_free( path );
}

static void disasm_table_ensure_visible( GtkTreeView *tree, int row )
{
    GtkTreePath *path = gtk_tree_path_new_from_indices( row, -1 );
    gtk_tree_view_scroll_to_cell( tree, path, NULL, FALSE, 0.0f, 0.0f );
    gtk_tree_path_free( path );
}

/************************** Register list table rows *****************************/
enum RegisterPageColumns {
    REG_COLUMN_NAME,
    REG_COLUMN_VALUE,
	REG_COLUMN_TEXTCOLOR,
    REG_COLUMN_COUNT
};

static void reglist_table_add_row( GtkListStore *list, const struct reg_desc_struct *reg, void *value )
{
    char buf[80];
    GtkTreeIter iter;

    if( reg->type == REG_TYPE_INT ) {
        sprintf( buf, "%08X", *((uint32_t *)value) );
    } else if( reg->type == REG_TYPE_FLOAT ) {
        sprintf( buf, "%f",   *((float *)value) );
    } else {
        sprintf( buf, "???" );
    }

    gtk_list_store_append( list, &iter );
    gtk_list_store_set(
        list, &iter,
        REG_COLUMN_NAME,      reg->name,
        REG_COLUMN_VALUE,     buf,
        REG_COLUMN_TEXTCOLOR, NULL,
        -1
    );
}

static void reglist_table_update_row( GtkTreeView *tree, int row, char* buf )
{
    GtkTreeModel *model = gtk_tree_view_get_model( tree );
    GtkListStore *list  = GTK_LIST_STORE( model );

    GtkTreeIter iter;
    assert(gtk_tree_model_iter_nth_child( model, &iter, NULL, row ));

    gtk_list_store_set(
        list, &iter,
        REG_COLUMN_VALUE,     buf,
        REG_COLUMN_TEXTCOLOR, &gui_colour_changed,
        -1
    );
}

static void reglist_table_reset_row( GtkTreeView *tree, int row )
{
    GtkTreeModel *model = gtk_tree_view_get_model( tree );
    GtkListStore *list  = GTK_LIST_STORE( model );

    GtkTreeIter iter;
    assert(gtk_tree_model_iter_nth_child( model, &iter, NULL, row ));

    gtk_list_store_set(
        list, &iter,
        REG_COLUMN_TEXTCOLOR, NULL,
        -1
    );
}


/************************** Register list table *****************************/
static void reglist_table_add_column( GtkTreeView *list, GtkCellRenderer *renderer, const char* title, int column, int width )
{
    gtk_tree_view_insert_column_with_attributes( list, column, title, renderer, 
                                                "text", column, 
                                                "foreground-gdk", REG_COLUMN_TEXTCOLOR, 
                                                NULL );
    GtkTreeViewColumn *col = gtk_tree_view_get_column( list, column );
    gtk_tree_view_column_set_sizing(    col, GTK_TREE_VIEW_COLUMN_FIXED );
    gtk_tree_view_column_set_min_width( col, width );
}

static void reglist_table_add( debug_window_t data, GtkWidget *hpaned )
{
    GtkCellRenderer *renderer;
    GtkListStore* store;

    GtkWidget *reg_scroll = gtk_scrolled_window_new (NULL, NULL);
    gtk_paned_pack2 (GTK_PANED (hpaned), reg_scroll, FALSE, TRUE);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (reg_scroll), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (reg_scroll), GTK_SHADOW_IN);

    store = GTK_LIST_STORE(
        gtk_list_store_new(REG_COLUMN_COUNT, G_TYPE_STRING, G_TYPE_STRING, GDK_TYPE_COLOR)
    );
    data->regs_list = GTK_TREE_VIEW(gtk_tree_view_new_with_model(GTK_TREE_MODEL(store)));

    renderer = gtk_cell_renderer_text_new();
    gtk_cell_renderer_set_padding( renderer, 0, 0 );
	gtk_tree_view_set_headers_visible( data->regs_list, FALSE );

    reglist_table_add_column( data->regs_list, renderer, _("Name"),  REG_COLUMN_NAME,  60 );
    reglist_table_add_column( data->regs_list, renderer, _("Value"), REG_COLUMN_VALUE, 70 );
    gtk_widget_modify_font( GTK_WIDGET(data->regs_list), gui_fixed_font );

    gtk_container_add (GTK_CONTAINER (reg_scroll), GTK_WIDGET(data->regs_list));
}

static void init_registers_list( debug_window_t data ) 
{
    int i;
    GtkTreeModel *model = gtk_tree_view_get_model( data->regs_list );
    GtkListStore *list  = GTK_LIST_STORE( model );
    gtk_list_store_clear( list );

    for( i=0; data->cpu->regs_info[i].name != NULL; i++ ) {
        void *value = data->cpu->get_register(i);
        if( value != NULL ) {
            reglist_table_add_row( list, &data->cpu->regs_info[i], value );
        }
    }
}

static void update_registers_list( debug_window_t data ) 
{
    int i;
    int posn = 0;
    char buf[80];

    for( i=0; data->cpu->regs_info[i].name != NULL; i++ ) {
        void *value = data->cpu->get_register(i);
        if( value != NULL ) {
            if( data->cpu->regs_info[i].type == REG_TYPE_INT ) {
                /* Yes this _is_ probably fairly evil */
                if( *((uint32_t *)value) !=
                    *((uint32_t *)((char *)data->saved_regs + ((char *)value - (char *)data->cpu->regs))) ) {
                    sprintf( buf, "%08X", *((uint32_t *)value) );
                    reglist_table_update_row( data->regs_list, posn, buf );
                } else {
                    reglist_table_reset_row( data->regs_list, posn );
                }
                posn++;
            } else if( data->cpu->regs_info[i].type == REG_TYPE_FLOAT ) {
                if( *((float *)value) !=
                    *((float *)((char *)data->saved_regs + ((char *)value - (char *)data->cpu->regs))) ) {
                    sprintf( buf, "%f", *((float *)value) );
                    reglist_table_update_row( data->regs_list, posn, buf );
                } else {
                    reglist_table_reset_row( data->regs_list, posn );
                }
                posn++;
            }
        }
    }

    set_disassembly_pc( data, *data->cpu->pc, TRUE );
    memcpy( data->saved_regs, data->cpu->regs, data->cpu->regs_size );
}


/*************************** Overall debug window ******************************/
static gboolean on_debug_delete_event(GtkWidget *widget, GdkEvent event, gpointer user_data)
{
    gtk_widget_hide( widget );
    return TRUE;
}

debug_window_t debug_window_new( const gchar *title, GtkWidget *menubar, 
                                 GtkWidget *toolbar, GtkAccelGroup *accel_group )
{
    debug_window_t data = g_malloc0( sizeof(struct debug_window_info) + cpu_list[0]->regs_size );
    GtkWidget *vbox;

    data->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_default_size (GTK_WINDOW (data->window), 700, 480);
    gtk_window_set_title( GTK_WINDOW(data->window), title );
    gtk_window_add_accel_group (GTK_WINDOW (data->window), accel_group);

    gtk_toolbar_set_style( GTK_TOOLBAR(toolbar), GTK_TOOLBAR_ICONS );

    data->statusbar = gtk_statusbar_new();

    GtkWidget *hpaned = gtk_hpaned_new ();
    gtk_paned_set_position (GTK_PANED (hpaned), 520);

    GtkWidget *disasm_box = gtk_vbox_new(FALSE,0);
    gtk_paned_pack1 (GTK_PANED (hpaned), disasm_box, TRUE, TRUE);

    disasm_controls_add( data, disasm_box );
    disasm_table_add( data, disasm_box );
    reglist_table_add( data, hpaned );

    vbox = gtk_vbox_new(FALSE, 0);
    gtk_container_add( GTK_CONTAINER(data->window), vbox );
    gtk_box_pack_start( GTK_BOX(vbox), menubar, FALSE, FALSE, 0 );
    gtk_box_pack_start( GTK_BOX(vbox), toolbar, FALSE, FALSE, 0 );
    gtk_box_pack_start( GTK_BOX(vbox), hpaned, TRUE, TRUE, 0 );
    gtk_box_pack_start( GTK_BOX(vbox), data->statusbar, FALSE, FALSE, 0 );

    g_signal_connect ((gpointer) data->window, "delete_event",
                      G_CALLBACK (on_debug_delete_event),
                      data);

    data->disasm_from = -1;
    data->disasm_to = -1;
    data->disasm_pc = -1;
    data->cpu = cpu_list[0];
    data->cpu_list = cpu_list;

    init_registers_list( data );
    g_object_set_data( G_OBJECT(data->window), "debug_data", data );
    set_disassembly_pc( data, *data->cpu->pc, FALSE );
    debug_window_set_running( data, FALSE );

    gtk_widget_show_all( data->window );
    return data;
}

void debug_window_show( debug_window_t data, gboolean show )
{
    if( show ) {
        gtk_widget_show( data->window );
    } else {
        gtk_widget_hide( data->window );
    }
}

/*
 * Check for changed registers and update the display
 */
void debug_window_update( debug_window_t data )
{
    update_registers_list( data );
    set_disassembly_pc( data, *data->cpu->pc, TRUE );
    memcpy( data->saved_regs, data->cpu->regs, data->cpu->regs_size );
}

void set_disassembly_region( debug_window_t data, unsigned int page )
{
    uint32_t i, posn, next;
    const char* symname;
    size_t symoffset;
    struct sym_table* symtab;	
    char buf[128];
    char addr[10];
    char opcode[16] = "";
    char *arr[4] = { addr, " ", opcode, buf };
    unsigned int from = page & 0xFFFFF000;
    unsigned int to = from + 4096;

    disasm_table_clear_rows( data->disasm_list );
    disasm_controls_page_set_addr( data, from );
    posn   = 0;
    symtab = data->cpu->getsymtable_func();

    if( !data->cpu->is_valid_page_func( from ) ) {
        disasm_table_add_unmapped_row( data->disasm_list, from );
    } else {
        for( i=from; i<to; i = next ) {
            next = data->cpu->disasm_func( i, buf, sizeof(buf), opcode );
            sprintf( addr, "%08X", i );

            if( symtab && (symname = sym_table_get_symbol( symtab, i )) ) {
                symoffset = strlen(buf);
                snprintf( buf + symoffset, sizeof(buf) - symoffset - 1, " ; %s", symname );
            }

            disasm_table_add_row( data->disasm_list, arr );
            if( buf[0] == '?' )
                disasm_table_forecolor_row( data->disasm_list, posn, &gui_colour_warn );
            if( data->disasm_pc == i )
                disasm_table_forecolor_row( data->disasm_list, posn, &gui_colour_pc );

            if( data->cpu->get_breakpoint != NULL ) {
                int type = data->cpu->get_breakpoint( i );
                switch(type) {
                case BREAK_ONESHOT:
                    disasm_table_backcolor_row( data->disasm_list, posn, &gui_colour_temp_break );
                    break;
                case BREAK_KEEP:
                    disasm_table_backcolor_row( data->disasm_list, posn, &gui_colour_break );
                    break;
                }
            }
            posn++;
        }
    }

    if( page != from ) { /* not a page boundary */
        disasm_table_ensure_visible( data->disasm_list, (page-from)>>1 );
    }
    data->disasm_from = from;
    data->disasm_to = to;
}

void jump_to_disassembly( debug_window_t data, unsigned int addr, gboolean select )
{
    int row;

    if( addr < data->disasm_from || addr >= data->disasm_to )
        set_disassembly_region(data,addr);

    row = address_to_row( data, addr );
    if( select ) {
        disasm_table_select_row( data->disasm_list, row );
    }
    disasm_table_ensure_visible( data->disasm_list, row );
}

void jump_to_pc( debug_window_t data, gboolean select )
{
    jump_to_disassembly( data, *data->cpu->pc, select );
}

void set_disassembly_pc( debug_window_t data, unsigned int pc, gboolean select )
{
    int cur_pc = data->disasm_pc;
    int row;

    jump_to_disassembly( data, pc, select );
    if( cur_pc != -1 && cur_pc >= data->disasm_from && cur_pc < data->disasm_to ) {
        row = address_to_row( data, cur_pc );
        disasm_table_forecolor_row( data->disasm_list, row, NULL );
    }

    row = address_to_row( data, pc );
    disasm_table_forecolor_row( data->disasm_list, row, &gui_colour_pc );
    data->disasm_pc = pc;
}

void set_disassembly_cpu( debug_window_t data, const gchar *cpu )
{
    int i;
    for( i=0; data->cpu_list[i] != NULL; i++ ) {
        if( strcmp( data->cpu_list[i]->name, cpu ) == 0 ) {
            if( data->cpu != data->cpu_list[i] ) {
                data->cpu = data->cpu_list[i];
                data->disasm_from = data->disasm_to = -1; /* Force reload */
                set_disassembly_pc( data, *data->cpu->pc, FALSE );
                init_registers_list( data );
            }
            return;
        }
    }
}

static void debug_window_toggle_breakpoint( debug_window_t data, int row )
{
    uint32_t pc = row_to_address( data, row );
    int oldType = data->cpu->get_breakpoint( pc );
    if( oldType != BREAK_NONE ) {
        data->cpu->clear_breakpoint( pc, oldType );
        disasm_table_backcolor_row( data->disasm_list, row, NULL );
    } else {
        data->cpu->set_breakpoint( pc, BREAK_KEEP );
        disasm_table_backcolor_row( data->disasm_list, row, &gui_colour_break );
    }
}

static void debug_window_set_oneshot_breakpoint( debug_window_t data, int row )
{
    uint32_t pc = row_to_address( data, row );
    data->cpu->clear_breakpoint( pc, BREAK_ONESHOT );
    data->cpu->set_breakpoint( pc, BREAK_ONESHOT );
    disasm_table_backcolor_row( data->disasm_list, row, &gui_colour_temp_break );
}

/**
 * Execute a single instruction using the current CPU mode.
 */
static void debug_window_single_step( debug_window_t data )
{
    data->cpu->step_func();
    gtk_gui_update();
}

uint32_t row_to_address( debug_window_t data, int row ) {
    return data->cpu->instr_size * row + data->disasm_from;
}

int address_to_row( debug_window_t data, uint32_t address ) {
    if( data->disasm_from > address || data->disasm_to <= address )
        return -1;
    return (address - data->disasm_from) / data->cpu->instr_size;
}

void debug_window_set_running( debug_window_t data, gboolean isRunning ) 
{
    if( data != NULL ) {
        gtk_gui_enable_action( "SingleStep", !isRunning );
        gtk_gui_enable_action( "RunTo", !isRunning && dreamcast_can_run() );
    }
}


/************************** Action callbacks *****************************/
void debug_memory_action_callback( GtkAction *action, gpointer user_data)
{
    gchar *title = g_strdup_printf( "%s :: %s", lxdream_package_name, _("Memory dump") );
    dump_window_new( title );
    g_free(title);
}

void debug_mmio_action_callback( GtkAction *action, gpointer user_data)
{
    gtk_gui_show_mmio();
}

void debug_step_action_callback( GtkAction *action, gpointer user_data)
{
    debug_window_single_step(gtk_gui_get_debugger());
}

void debug_runto_action_callback( GtkAction *action, gpointer user_data)
{
    debug_window_t debug = gtk_gui_get_debugger();
    int selected_row = disasm_table_get_selected_row(debug);
    if( selected_row == -1 ) {
        WARN( _("No address selected, so can't run to it"), NULL );
    } else {
        debug_window_set_oneshot_breakpoint( debug, selected_row );
        dreamcast_run();
    }
}

void debug_breakpoint_action_callback( GtkAction *action, gpointer user_data)
{
    debug_window_t debug = gtk_gui_get_debugger();
    int selected_row = disasm_table_get_selected_row(debug);
    if( selected_row != -1 ) {
        debug_window_toggle_breakpoint( debug, selected_row );
    }
}
